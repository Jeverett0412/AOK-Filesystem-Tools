#!/bin/sh
#  shellcheck disable=SC2154
#
#  Part of https://github.com/emkey1/AOK-Filesystem-Tools
#
#  License: MIT
#
#  This enhances an Alpine Linux FS with the AOK changes
#
#  On compatible platforms, Linux (x86) and iSH this can be run chrooted
#  before compressing the file system, to deliver a ready to be used file system.
#  When the FS is prepared on other platforms,
#  this file has to be run inside iSH once the file system has been mounted.
#
version="1.3.0"

prog_name=$(basename "$0")

echo "$prog_name, version $version"
echo

if [ ! -d /AOK ]; then
    echo "ERROR: This is not an AOK File System!"
    echo
    exit 1
fi


#
#  This is run chrooted inside the iSH filesystem
#

# Set some variables
# shellcheck disable=SC1091
. /AOK/BUILD_ENV

has_been_run="/etc/opt/aok_setup_fs-done"


echo "---  Populating $AOK_CONTENT on new filesystem  ---"

if [ -e "$has_been_run" ]; then
    echo "ERROR: $SETUP_AOK_FS has already been run on this FS!"
    echo
    exit 1
fi

echo
echo "=====  Setting up AOK FS  ====="
echo

if [ -z "$ALPINE_RELEASE" ]; then
    echo "ERROR: ALPINE_RELEASE param not supplied"
    exit 1
fi


echo "---  Initialize APK Database  ---"

# apk update
apk -U upgrade


if [ -n "$CORE_APKS" ]; then
    echo
    echo "---  Add initial packages  ---"

    # In this case we want the variable to expand into its components
    # shellcheck disable=SC2086
    apk add $CORE_APKS
fi


if [ -n "$AOK_APKS" ]; then
    echo
    echo "---  Add AOK only packages  ---"
    # In this case we want the variable to expand into its components
    # shellcheck disable=SC2086
    apk add $AOK_APKS
fi


echo
echo "---  Replacing a few key files  ---"

ln /etc/init.d/devfs /etc/init.d/dev

# Fake interfaces file
cp "$AOK_CONTENT"/Files/interfaces /etc/network

# Move sshd to port 1022 to avoid issues
sshd_port=1022
echo "sshd listening on port: $sshd_port"
sed -i "s/.*Port .*/Port $sshd_port/" /etc/ssh/sshd_config

# /etc/init.d/networking keeps getting screwed up for unknown reasons.
# Mitigate
mkdir /root/init.d
cp "$AOK_CONTENT"/Files/init.d/* /root/init.d

# Remove extra unused vty's, make OpenRC work
cp "$AOK_CONTENT"/Files/inittab /etc

# Custom MOTD/issue
sed "s/AOK_VERSION/$AOK_VERSION/" "$AOK_CONTENT"/Files/motd > /etc/motd
sed "s/AOK_VERSION/$AOK_VERSION/" "$AOK_CONTENT"/Files/issue > /etc/issue


# Sudoers stuff, group wheel can do sudo without password
echo "activating group wheel for passwordless sudo"
sed -i "s/# %wheel ALL=(ALL) NOPASSWD.*/%wheel ALL=(ALL) NOPASSWD: ALL/" /etc/sudoers


# Root .profile.
cp "$AOK_CONTENT"/Files/profile /etc/profile

# Add some simple scripts
cp "$AOK_CONTENT"/Files/bin/* /usr/local/bin

# cron stuff
cp "$AOK_CONTENT"/Files/cron/15min/* /etc/periodic/15min
#cp Files/cron/daily/* /etc/periodic/daily
#cp Files/cron/hourly/* /etc/periodic/hourly
#cp Files/cron/monthly/* /etc/periodic/monthly
#cp Files/cron/weekly/* /etc/periodic/weekly

mkdir -p /usr/local/sbin
cp "$AOK_CONTENT"/Files/sbin/* /usr/local/sbin

# Make a link so that showip can also be 'myip'
ln /usr/local/bin/showip /usr/local/bin/myip

chmod +x /usr/local/bin/*


echo "---  Creating the ish user and group  ---"

groupadd -g 501 ish

# temp changing UID_MIN is to silence the warning:
# ish's uid 501 outside of the UID_MIN 1000 and UID_MAX 6000 range.
#  add additional groups with -G
useradd -m -s /bin/bash -u 501 -g 501 -G wheel,root,adm ish --key UID_MIN=501

# Add dot files for ish
cp "$AOK_CONTENT"/Files/bash_profile /home/ish/.bash_profile; chown ish:ish /home/ish/.bash_profile
ln -s /home/ish/.bash_profile /home/ish/.bashrc
chown ish:ish /home/ish/.bashrc
cp "$AOK_CONTENT"/Files/zshrc /home/ish/.zshrc; chown ish:ish /home/ish/.zshrc
cp "$AOK_CONTENT"/Files/vimrc /home/ish/.vimrc; chown ish:ish /home/ish/.vimrc
# Copy to root as well
cp "$AOK_CONTENT"/Files/bash_profile /root/.bash_profile
ln -s /root/.bash_profile /root/.bashrc
cp "$AOK_CONTENT"/Files/zshrc /root/.zshrc
cp "$AOK_CONTENT"/Files/vimrc /root/.vimrc

# shadow with blank ish password
sed -i "s/ish:\!:/ish::/" /etc/shadow


# Copy some documentation to the ish user home directory


echo "---  Copying Docs  ---"

mkdir /home/ish/Docs
cp -r "$AOK_CONTENT"/Docs/* /home/ish/Docs
chown -R ish:ish /home/ish/Docs


echo "---  Setting up environment  ---"


# Networking, hostname and possibly others can't start because of
# current limitations in iSH So we fake it out
rm /etc/init.d/networking
cp "$AOK_CONTENT"/Files/init.d/* /etc/init.d

# More hackery.  Initial case is the need to make pam_motd.so optional
# So that the ish user will work in Alpine 3.14
cp "$AOK_CONTENT"/Files/pam.d/* /etc/pam.d

cp "$AOK_CONTENT"/Files/tmux.conf /home/ish/.tmux.conf
chown ish:ish /home/ish/.tmux.conf
cp "$AOK_CONTENT"/Files/tmux.conf /root/.tmux.conf

# I want vim to be vi, aliasing isn't working for some reason
# /usr/local/bin is first in the path by default, so...
ln -s /usr/bin/vim /usr/local/bin/vi

ln /usr/share/zoneinfo/America/Los_Angeles /etc/localtime
echo "$INITIAL_TIMEZONE" >  /etc/timezone

# Change roots shell
sed -i 's/\/bin\/ash$/\/bin\/bash/' /etc/passwd

echo "$AOK_VERSION" > /etc/aok-release

echo "---  Initial login method  ---"
cp "$AOK_CONTENT"/Files/login.loop /bin
cp "$AOK_CONTENT"/Files/login.once /bin

# chmod +x /bin/login.alpine
chmod +x /bin/login.loop
chmod +x /bin/login.once

#
#  3.16 no longer has a separate login binary, it just soft links to busybox
#  and fails when running the alternate login bins. Just keeps printing
#  the login promppt in an endless loop
#
if [ -h /bin/login ]; then
    echo
    echo "WARNING: Alpine $ALPINE_RELEASE does not support changing login method"
    echo
else
    mv /bin/login /bin/login.alpine

    if ! /usr/local/bin/aok -l "$INITIAL_LOGIN_MODE"; then
        echo "ERROR: Failed to set up INITIAL_LOGIN_MODE [$INITIAL_LOGIN_MODE]"
        exit 1
    fi
fi

#
#  Extra sanity check, only continue if there is a runable /bin/login
#
if [ ! -x /bin/login ]; then
    echo "ERROR: no run-able /bin/login present!"
    exit 1
fi


echo "---  Enabling openrc  ---"

cp "$AOK_CONTENT"/Files/rc.conf /etc
mkdir /run/openrc
touch /run/openrc/softlevel

#
# Older Alpines needs to run openrc-init
#
case "$ALPINE_RELEASE" in

    "3.12" | "3.13" | "3.14" | "3.15")
        openrc-init
        ;;

    *) ;;

esac


# Set openrc boot level to be default
echo "Setting OpenRC runlevel to 'default'"
echo "default" > /run/openrc/softlevel

echo
echo "=====  AOK FS is ready  ====="
if [ "$build_env" -eq 1 ]; then
    echo "Please restart the app for changes to take full effect!"
    echo
fi

#
#  Indicate this has been completed, to prevent future runs by mistake
#
touch "$has_been_run"
